# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, home-manager, ... }:

{
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  # Enable garbage collection
  nix.gc.automatic = true;

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.configurationLimit = 10;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # Set linux kernel version
  #boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelPackages = pkgs.linuxPackages_zen;

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  networking.hostName = "nixos"; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Oslo";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  #services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  services.xserver.displayManager.defaultSession = "plasmawayland";
  services.xserver.displayManager.gdm.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Make non root processes able to communicate with root processes
  security.polkit.enable = true;
  services.dbus.packages = [ pkgs.libsForQt5.kpmcore ];

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable bluetooth
  hardware.bluetooth.enable = true;

  hardware = {
    enableRedistributableFirmware = true;
    sensor.iio.enable = true;
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.oskar = {
    isNormalUser = true;
    description = "Oskar";
    extraGroups = [ "networkmanager" "wheel" "docker" "adbusers" "wireshark" "uucp" "dialout" ];
    packages =  with pkgs; [
      xclip   # Only needed on X11
      filelight
      librewolf
      unzip
      wl-clipboard
      firefox
      microsoft-edge
      docker-compose
      kate
      ark
      neofetch
      vim
      tree
      git
      tldr
      distrobox
      nextcloud-client
      smartmontools
      wireshark
      openssl
      gnupg
    ];
  };

  programs = {
    partition-manager.enable = true;
    rog-control-center.enable = true;
    rog-control-center.autoStart = true;
    kdeconnect.enable = true;
    wireshark.enable = true;
    adb.enable = true;
    xwayland.enable = true;
    dconf.enable = true;

    fish.shellAliases = {
      unzip-all = "find . -name \"*.zip\" | xargs -P 5 -I fileName sh -c 'unzip -o -d \"$(dirname \"fileName\")/$(basename -s .zip \"fileName\")\" \"fileName\"'";
      rebuild = "sudo nixos-rebuild switch --flake /home/oskar/.nixconf/";
      upgrade = "nix flake update ~/.nixconf/ --commit-lock-file && sudo nixos-rebuild switch --upgrade --flake /home/oskar/.nixconf/";
    };
  };

  # Install fonts
  fonts.fonts = with pkgs; [
      nerdfonts
      powerline-fonts
      powerline-symbols
      roboto
      roboto-mono
  ];
  fonts.fontconfig.defaultFonts.monospace = [ "RobotoMono Nerd Font Mono" ];

  powerManagement.powertop.enable = true;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    libsForQt5.breeze-gtk
    libsForQt5.packagekit-qt
    libsForQt5.discover
    libsForQt5.kde-gtk-config
    libsForQt5.kgpg
    vim
    bash
    sbctl
    pciutils
    libsForQt5.kpmcore
    partition-manager
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Set fish as default shell
  programs.fish.enable = true;
  users.defaultUserShell = pkgs.fish;
  environment.shells = with pkgs; [ zsh ];

  virtualisation.docker.enable = true;

  services = {
    flatpak.enable = true;
    packagekit.enable = true;
    packagekit.settings = {
      Daemon = {
        DefaultBackend = "dummy";
        KeepCache = false;
      };
    };
    lorri.enable = true;    # A nix-shell replacement
    fwupd.enable = true;
    asusd.enable = true;
    supergfxd.enable = true;
  };

  xdg.portal.enable = true;


  # Nvidia drivers
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.opengl.enable = true;
  #hardware.opengl.mesaPackage = pkgs.mesa;             # Temporary fix for wayland issues
  hardware.opengl.extraPackages = with pkgs; [ amdvlk ];
  hardware.nvidia.modesetting.enable = true;
  hardware.nvidia.powerManagement.finegrained = true;

  hardware.nvidia.prime = {
   offload.enable = true;
   offload.enableOffloadCmd = true;
   amdgpuBusId = "PCI:08:00:0";
   nvidiaBusId = "PCI:01:00:0";
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}
