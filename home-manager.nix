{ config, lib, pkgs, home-manager, ... }:

{
  home-manager.users.oskar = {
    home.packages = with pkgs; [
      protonvpn-gui
      vscode
      arduino-cli
      arduino-mk

      # Emacs deps
      (python3.withPackages(ps: with ps; [
        matplotlib
        mypy
        python-lsp-server
        pytest
        nose
        black
        pyflakes
        pygments
      ]))

      direnv
      ripgrep
      fd
      pdf2svg
      texlive.combined.scheme-full
      plantuml
      texlab
      lua53Packages.digestif
      pandoc
      imagemagick
      exercism
      cmake
      gcc
      valgrind
      gdb
      glibc
      man-pages
      maude
      julia
      rustc
      rust-analyzer
      libtool
      gnumake
      libvterm
      direnv
      elixir
      editorconfig-core-c
      sqlite
      openjdk
      gradle
      nodejs
      clang-tools
      glslang
      wakatime
      hunspell
      hunspellDicts.nb-no
      hunspellDicts.en_US
      haskell.compiler.ghc943
      haskell-language-server
    ];

    programs = {

      emacs = {
        enable = true;
        package = pkgs.emacs-gtk;
      };

      git = {
        enable = true;
        userName  = "ohaukeboe";
        userEmail = "ohaukeboe@pm.me";
      };

    };

    home.sessionVariables = {
      DIRENV_LOG_FORMAT="";
    };


    systemd.user.services.emacs.Unit = {
      After = [ "graphical-session-pre.target" ];
      PartOf = [ "graphical-session.target" ];
    };


    home.stateVersion = "22.11";
  };
}
